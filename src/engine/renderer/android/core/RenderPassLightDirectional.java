package engine.renderer.android.core;

import android.opengl.GLES20;
import engine.renderer.android.object.GLESBuffer;
import engine.renderer.android.object.GLESTexture;
import engine.renderer.android.shader.lightpass.ShaderLightPassDirectional;
import engine.scene.Light;
import engine.terrain.Terrain;
import geometry.math.Camera;


public class RenderPassLightDirectional extends RenderPassLightGeneric
	<ShaderLightPassDirectional>{//, ShaderTerrainLightPassDirectional>{


	public RenderPassLightDirectional(GLES20 gl){
		super(
			new ShaderLightPassDirectional(gl, false), 
			new ShaderLightPassDirectional(gl, true)//,
			//new ShaderTerrainLightPassDirectional(gl)
			);
	}
	
	@Override
	public void initializePass(GLES20 gl, Light light, Camera camera, float fogDensity){
		super.initializePass(gl, light, camera, fogDensity);
		
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GLES20,GLESBuffer,GLESTexture> terrain){
		super.actionTerrainBegin(terrain);
		
		//[TODO]
		//shaderTerrain.setAmbientToShadowMapTransformation(gl, light);
	}
	
	@Override
	protected boolean bindShader(){
		if(!super.bindShader()){ return false; }
		
		shader.setAmbientToShadowMapTransformation(gl, light);
		return true;
	}
	
	@Override
	protected boolean bindShaderBump(){
		if(!super.bindShaderBump()){ return false; }
		
		shaderBump.setAmbientToShadowMapTransformation(gl, light);
		return true;
	}
}
