package engine.renderer.android.core;

import android.opengl.GLES20;
import engine.renderer.DrawableIBO;
import engine.renderer.android.object.GLESBuffer;
import engine.renderer.android.object.GLESTexture;
import engine.renderer.android.shader.ShaderProgram;
import engine.renderer.android.shader.lightpass.ShaderShadowMap;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;

public class RenderPassShadowMap 
	extends SceneTraversal<GLES20,GLESBuffer,GLESTexture>{
	
	private static final boolean CHECK_VALIDATION = false; //[TODO] for debugging

	private ShaderShadowMap shaderMask;
	private ShaderShadowMap shaderNoMask;
	//TODO private ShaderTerrainShadowMap shaderTerrain;
	
	//transient data for a single pass
	protected GLES20 gl;
	protected Camera camera;
	
	//things that can change; store last so not flipped every object
	protected GLESTexture lastActiveTexColor;
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassShadowMap(GLES20 gl){
		shaderMask = new ShaderShadowMap(gl, true);
		shaderNoMask = new ShaderShadowMap(gl, false);
		//TODO shaderTerrain = new ShaderTerrainShadowMap(gl);
	}
	
	public void initializePass(GLES20 gl, Camera camera){
		this.gl = gl;
		this.camera = camera;
		
		//no active texture
		lastActiveTexColor = null;
		lastActiveObjectShader = null;

		setupShadowMapPass();
	}
	
	private void setupShadowMapPass(){
		
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		GLES20.glDepthFunc(GLES20.GL_LESS);
		
		GLES20.glDepthMask(true); 
		GLES20.glColorMask(false, false, false, false);
		
		GLES20.glDisable(GLES20.GL_BLEND);
		
		//GLES20.glEnable(GLES20.GL_CULL_FACE);
		//GLES20.glCullFace(GLES20.GL_BACK);
	}
	
	@Override
	public void actionObject(SceneObject<GLES20,GLESBuffer,GLESTexture> sceneObject){
		
		if(sceneObject.getMaterial().hasAlphaMask()){
			bindShaderMask();
			bindTexColor(sceneObject.getMaterial().getTexColor());
			
			shaderMask.setObjectTransformation(gl, sceneObject);
			shaderMask.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}else{
			bindShaderNoMask();
			
			shaderNoMask.setObjectTransformation(gl, sceneObject);
			shaderNoMask.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}

		if(CHECK_VALIDATION){ lastActiveObjectShader.checkValidateStatus(gl); } //[TODO]
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3*sceneObject.getDrawable().getTriCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GLES20,GLESBuffer,GLESTexture> terrain){
		
		//[TODO]
		/*//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);*/
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<GLES20,GLESBuffer> tileIBO, Box<Vector2d> tileLerp) {
		
		//[TODO]
		/*shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getIndexBuffer().bindAsElementArray(gl);
		
		if(CHECK_VALIDATION){ shaderTerrain.checkValidateStatus(gl); } //[TODO]
		GLES20.glDrawElements(GLES20.GL_TRIANGLES, 3*tileIBO.getTriCount(), GLES20.GL_UNSIGNED_SHORT, 0);*/
	}

	@Override
	public void actionTerrainEnd(Terrain<GLES20,GLESBuffer,GLESTexture> terrain) {
		
		//[TODO]
		/*shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);*/
	}
	
	protected boolean bindTexColor(GLESTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexColor){ return false; }
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderShadowMap.COLOR_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderShadowMap.COLOR_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindShaderMask(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderMask){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderMask.useShader(gl);
		shaderMask.setViewMatrices(gl, camera);
		
		lastActiveObjectShader = shaderMask;
		return true;
	}
	
	protected boolean bindShaderNoMask(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderNoMask){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderNoMask.useShader(gl);
		shaderNoMask.setViewMatrices(gl, camera);
		
		lastActiveObjectShader = shaderNoMask;
		return true;
	}
	
	public void finishPass(){
		
		//unbind texture and shader, if there is one bound
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderShadowMap.COLOR_TEXTURE_UNIT); }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		camera = null;
	}
}
