package engine.renderer.android.core;

import android.opengl.GLES20;
import engine.renderer.DrawableIBO;
import engine.renderer.Material;
import engine.renderer.android.object.GLESBuffer;
import engine.renderer.android.object.GLESTexture;
import engine.renderer.android.shader.ShaderProgram;
import engine.renderer.android.shader.lightpass.ShaderLightPass;
import engine.scene.Light;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;


public class RenderPassLightGeneric
	<ShaderObject extends ShaderLightPass>//,
	 //ShaderTerrain extends ShaderTerrainLightPass>
	extends SceneTraversal<GLES20,GLESBuffer,GLESTexture>{
	
	private static final boolean CHECK_VALIDATION = false; //[TODO] for debugging
	
	protected ShaderObject shader;
	protected ShaderObject shaderBump;
	//[TODO] protected ShaderTerrain shaderTerrain;
	
	//transient data for a single pass
	protected GLES20 gl;
	protected Light light;
	protected Camera camera;
	protected float fogDensity;
	
	//things that can change; store last so not flipped every object
	protected GLESTexture lastActiveTexColor, lastActiveTexBump;
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassLightGeneric(ShaderObject shader, ShaderObject shaderBump){//, ShaderTerrain shaderTerrain){
		
		this.shader = shader;
		this.shaderBump = shaderBump;
		//[TODO] this.shaderTerrain = shaderTerrain;
	}
	
	public void initializePass(GLES20 gl, Light light, Camera camera, float fogDensity){
		this.gl = gl;
		this.light = light;
		this.camera = camera;
		this.fogDensity = fogDensity;
		
		//no active shader or texture
		lastActiveObjectShader = null;
		lastActiveTexColor = null;
		lastActiveTexBump = null;
		
		setupLightPass();
	}
	
	private void setupLightPass(){
		
		GLES20.glEnable(GLES20.GL_DEPTH_TEST);
		GLES20.glDepthFunc(GLES20.GL_LEQUAL);
		
		GLES20.glDepthMask(false); 
		GLES20.glColorMask(true, true, true, false);
		
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE);
		
		//gl.glEnable(GL2ES2.GL_CULL_FACE);
		//gl.glCullFace(GL2ES2.GL_BACK);
	}
	
	@Override
	public void actionObject(SceneObject<GLES20,GLESBuffer,GLESTexture> sceneObject){

		Material<GLESTexture> mtl = sceneObject.getMaterial();
		
		//bind appropriate shader and texture, if necessary, then draw
		if(mtl.hasTexBump() && sceneObject.getDrawable().hasTangentData()){
			bindShaderBump();
			bindTexColor(mtl.getTexColor());
			
			bindTexBump(mtl.getTexBump());
			shaderBump.setReliefMappingHeight(gl, mtl.getReliefMappingHeight());
			
			shaderBump.setMaterialLightingCoefficients(gl, mtl);
			shaderBump.setObjectTransformation(gl, sceneObject);
			shaderBump.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}else{
			bindShader();
			bindTexColor(mtl.getTexColor());
			
			shader.setMaterialLightingCoefficients(gl, mtl);
			shader.setObjectTransformation(gl, sceneObject);
			shader.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}
		
		if(CHECK_VALIDATION){ lastActiveObjectShader.checkValidateStatus(gl); } //[TODO]
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3*sceneObject.getDrawable().getTriCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GLES20,GLESBuffer,GLESTexture> terrain){
		
		//[TODO]
		/*
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		//pass-specific shader uniforms
		shaderTerrain.setLightAndFog(gl, light, fogDensity);
		shaderTerrain.setMaterialLightingCoefficients(gl, terrain.getMaterial().getMaterial1());
		
		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		
		terrain.getDataNormalAndBlend().bind(gl, ShaderTerrainLightPass.NORMAL_AND_BLEND_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial1().getTexColor().bind(gl, ShaderTerrainLightPass.COLOR1_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial2().getTexColor().bind(gl, ShaderTerrainLightPass.COLOR2_TEXTURE_UNIT);
		*/
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<GLES20,GLESBuffer> tileIBO, Box<Vector2d> tileLerp) {
		
		//[TODO] 
		/*
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getIndexBuffer().bindAsElementArray(gl);
		
		if(CHECK_VALIDATION){ shaderTerrain.checkValidateStatus(gl); } //[TODO]
		gl.glDrawElements(GL2ES2.GL_TRIANGLES, 3*tileIBO.getTriCount(), GL2ES2.GL_UNSIGNED_SHORT, 0);
		*/
	}

	@Override
	public void actionTerrainEnd(Terrain<GLES20,GLESBuffer,GLESTexture> terrain) {
		
		//[TODO] 
		/*
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		
		terrain.getDataNormalAndBlend().unbind(gl, ShaderTerrainLightPass.NORMAL_AND_BLEND_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial1().getTexColor().unbind(gl, ShaderTerrainLightPass.COLOR1_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial2().getTexColor().unbind(gl, ShaderTerrainLightPass.COLOR2_TEXTURE_UNIT);
		*/
	}
	
	protected boolean bindTexColor(GLESTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexColor){ return false; }
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindTexBump(GLESTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexBump){ return false; }
		if(lastActiveTexBump != null){ lastActiveTexBump.unbind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindShader(){
		//bind shader if it's not already, first unbinding previous shader if there is one
		//returns false if it did not bind anything
		
		if(lastActiveObjectShader == shader){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shader.useShader(gl);
		shader.setViewMatrices(gl, camera);
		shader.setLightAndFog(gl, light, fogDensity);
		
		lastActiveObjectShader = shader;
		return true;
	}
	
	protected boolean bindShaderBump(){
		//bind shader if it's not already, first unbinding previous shader if there is one
		//returns false if it did not bind anything
		
		if(lastActiveObjectShader == shaderBump){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderBump.useShader(gl);
		shaderBump.setViewMatrices(gl, camera);
		shaderBump.setLightAndFog(gl, light, fogDensity);
		
		lastActiveObjectShader = shaderBump;
		return true;
	}
	
	protected void finishPass(){
		
		//unbind texture and shader, if there is one bound
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT); }
		if(lastActiveTexBump != null){ lastActiveTexBump.unbind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT); }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		light = null;
		camera = null;
	}
}
