package engine.renderer.android.core;

import android.opengl.GLES20;
import engine.renderer.DrawableIBO;
import engine.renderer.android.object.GLESBuffer;
import engine.renderer.android.object.GLESTexture;
import engine.renderer.android.shader.ShaderProgram;
import engine.renderer.android.shader.lightpass.ShaderWireframe;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;


public class RenderPassWireframe 
	extends SceneTraversal<GLES20,GLESBuffer,GLESTexture>{
	
	private ShaderWireframe shader;
	//[TODO] private ShaderTerrainWireframe shaderTerrain;
	
	//transient data for a single pass
	protected GLES20 gl;
	protected Camera camera;
	
	//things that can change; store last so not flipped every object
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassWireframe(GLES20 gl){
		shader = new ShaderWireframe(gl);
		//[TODO] shaderTerrain = new ShaderTerrainWireframe(gl);
	}

	public void initializePass(GLES20 gl, Camera camera){
		this.gl = gl;
		this.camera = camera;
		
		//no active shader
		lastActiveObjectShader = null;
		
		setupWireframePass();
	}
	
	private void setupWireframePass(){
		
		GLES20.glDisable(GLES20.GL_DEPTH_TEST);
		
		GLES20.glDepthMask(false);
		GLES20.glColorMask(true, true, true, false);
		
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
	}
	
	@Override
	public void actionObject(SceneObject<GLES20,GLESBuffer,GLESTexture> sceneObject){
		if(!sceneObject.getDrawable().hasWireframeData()){ return; }
		
		//bind shader, if necessary
		bindObjectShader();
				
		shader.setColor(gl, sceneObject.getDrawOptions().getWireframeColor());
		shader.setObjectTransformation(gl, sceneObject);
		shader.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		GLES20.glDrawArrays(GLES20.GL_LINES, 0, 2*sceneObject.getDrawable().getEdgeCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<GLES20,GLESBuffer,GLESTexture> terrain){

		//[TODO] 
		/*
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		shaderTerrain.setColor(gl, terrain.getDrawOptions().getWireframeColor());

		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		*/
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<GLES20,GLESBuffer> tileIBO, Box<Vector2d> tileLerp) {
		if(!tileIBO.hasWireframeData()){ return; }
		
		//[TODO]
		/*
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getWireframeIndexBuffer().bindAsElementArray(gl);
		
		GLES20.glDrawElements(GLES20.GL_LINES, 2*tileIBO.getEdgeCount(), GLES20.GL_UNSIGNED_SHORT, 0);
		*/
	}

	@Override
	public void actionTerrainEnd(Terrain<GLES20,GLESBuffer,GLESTexture> terrain) {
		
		//[TODO]
		/*
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		*/
	}
	
	protected boolean bindObjectShader(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shader){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shader.useShader(gl);
		shader.setViewMatrices(gl, camera);
		
		lastActiveObjectShader = shader;
		return true;
	}
	
	public void finishPass(){
		
		//unbind shader, if there is one bound
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		camera = null;
	}
}
