package engine.renderer.android.core;

import android.opengl.GLES20;
import engine.renderer.android.shader.lightpass.ShaderLightPass;


public class RenderPassLight extends RenderPassLightGeneric
	<ShaderLightPass>{//, ShaderTerrainLightPass>{

	public RenderPassLight(GLES20 gl){
		super(
			new ShaderLightPass(gl, false), 
			new ShaderLightPass(gl, true)
			//new ShaderTerrainLightPass(gl));
		);
	}
}
