package engine.renderer.android.shader;

import java.io.InputStream;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import android.opengl.GLES20;
import engine.renderer.shadertools.GLSLESPreprocessor;
import geometry.common.MessageOutput;


public abstract class ShaderProgram{
	private static final boolean CHECK_ERRORS = true;
	
	private String name; //just used for outputting debug info/errors
	
	protected int shaderProgramID;
	private ArrayList<Integer> vertexAttributes;
	
	//specified sources for preprocessing [only needed until finishAttaching is called]
	private InputStream primaryVertexShaderSource, primaryFragmentShaderSource;
	private HashMap<String,InputStream> vertexShaderSourceIdentifiers, fragmentShaderSourceIdentifiers;
	
	
	public ShaderProgram(GLES20 gl, String name){ 
		this.name = name;
		
		vertexAttributes = new ArrayList<Integer>();
		shaderProgramID = GLES20.glCreateProgram();
		
		vertexShaderSourceIdentifiers = new HashMap<String,InputStream>();
		fragmentShaderSourceIdentifiers = new HashMap<String,InputStream>();
	}
	
	
	protected void setPrimaryVertexShaderSource(InputStream source){
		primaryVertexShaderSource = source;
	}
	
	protected void setPrimaryFragmentShaderSource(InputStream source){
		primaryFragmentShaderSource = source;
	}
	
	protected void addVertexShaderSourceIdentifier(String identifier, InputStream source){
		vertexShaderSourceIdentifiers.put(identifier, source);
	}
	
	protected void addFragmentShaderSourceIdentifier(String identifier, InputStream source){
		fragmentShaderSourceIdentifiers.put(identifier, source);
	}
	
	
	protected void finishAttaching(GLES20 gl){
		
		//do preprocessing to link multiple sources (necessary because of GLES 2.0); attach resulting source
		String vertexShaderSource = GLSLESPreprocessor.process(primaryVertexShaderSource, vertexShaderSourceIdentifiers);
		VertexShader vs = new VertexShader(gl, vertexShaderSource);
		GLES20.glAttachShader(shaderProgramID, vs.getID());
		
		String fragmentShaderSource = GLSLESPreprocessor.process(primaryFragmentShaderSource, fragmentShaderSourceIdentifiers);
		FragmentShader fs = new FragmentShader(gl, fragmentShaderSource);
		GLES20.glAttachShader(shaderProgramID, fs.getID());
		
		//bind vertex attributes
		setVertexAttributes(gl);
		
		//link the shader program
		MessageOutput.printDebug("Linking program (" + name + ")");
		GLES20.glLinkProgram(shaderProgramID);
		if(CHECK_ERRORS){ checkLinkStatus(gl); }

		//set up uniforms that will be used
		setUniforms(gl);
	}

	
	protected abstract void setVertexAttributes(GLES20 gl);
	
	protected void addVertexAttribute(GLES20 gl, String name, int index){
		vertexAttributes.add(index);
		GLES20.glBindAttribLocation(shaderProgramID, index, name);
	}
	
	protected abstract void setUniforms(GLES20 gl);

	public void useShader(GLES20 gl){
		
		//enable shader
		GLES20.glUseProgram(shaderProgramID);
		//enable attributes
		for(int index : vertexAttributes){ GLES20.glEnableVertexAttribArray(index); }
	}
	
	public void unuseShader(GLES20 gl){
		
		//disable attributes
		for(int index : vertexAttributes){ GLES20.glDisableVertexAttribArray(index); }
		//disable shader
		GLES20.glUseProgram(0);
	}
	
    
    public void checkLinkStatus(GLES20 gl){
		
        IntBuffer status = IntBuffer.allocate(1);
        GLES20.glGetProgramiv(shaderProgramID, GLES20.GL_LINK_STATUS, status);

        MessageOutput.printDebug("Checking GL_LINK_STATUS (" + name + ")");
        if(status.get() == GLES20.GL_FALSE){
        	MessageOutput.printDebug("GL_LINK_STATUS is GL_FALSE! Info log: ");
        	throw new Error(getProgramInfoLog(gl));
        }else{
        	MessageOutput.printDebug("GL_LINK_STATUS is ok. Info log: \n" + getProgramInfoLog(gl));
        }
    }

    public void checkValidateStatus(GLES20 gl){
		
		IntBuffer status = IntBuffer.allocate(1); 
		GLES20.glValidateProgram(shaderProgramID);
		GLES20.glGetProgramiv(shaderProgramID, GLES20.GL_VALIDATE_STATUS, status);
		
		MessageOutput.printDebug("Checking GL_VALIDATE_STATUS (" + name + ")");
		if(status.get() == GLES20.GL_FALSE){
			MessageOutput.printDebug("GL_VALIDATE_STATUS is GL_FALSE! Info log: ");
			throw new Error(getProgramInfoLog(gl));
		}else{
			MessageOutput.printDebug("GL_VALIDATE_STATUS is ok. Info log: \n" + getProgramInfoLog(gl));
		}	
	}
    
    public String getProgramInfoLog(GLES20 gl){
   	 
        String infoLog = GLES20.glGetProgramInfoLog(shaderProgramID);

        return infoLog;
    }
}
