package engine.renderer.android.shader;

import java.nio.IntBuffer;

import android.opengl.GLES20;
import geometry.common.MessageOutput;


public abstract class Shader{
	protected static final boolean CHECK_ERRORS = true;
	
	protected int shaderID;
	
	
	protected Shader(int shaderID){
		this.shaderID = shaderID;
	}
	
	public int getID(){ return shaderID; }
	
	
	public void checkCompileStatus(GLES20 gl){
		
        IntBuffer status = IntBuffer.allocate(1);
        GLES20.glGetShaderiv(shaderID, GLES20.GL_COMPILE_STATUS, status);
        
        if(status.get() == GLES20.GL_FALSE){
        	MessageOutput.printDebug("GL_COMPILE_STATUS is GL_FALSE! Info log: ");
        	throw new Error(getShaderInfoLog(gl));
        }else{
        	MessageOutput.printDebug("GL_COMPILE_STATUS is ok. Info log: \n" + getShaderInfoLog(gl));
        }
    }
	
	public String getShaderInfoLog(GLES20 gl){
		
		//TODO
		//bug causes this to return nothing (see http://code.google.com/p/android/issues/detail?id=9953)
		//can work around it by using these bindings: http://code.google.com/p/gl2-android/
		
        //String infoLog = GLES20.glGetShaderInfoLog(shaderID);
		String infoLog = "No info";
        
        return infoLog;
    }

}
