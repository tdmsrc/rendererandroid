package engine.renderer.android.shader.quad;

import android.opengl.GLES20;

public class ShaderQuadColorAdjust extends ShaderQuad{

	//adjustment parameters
	private int colorAdjustBrightnessUniform;
	private int colorAdjustContrastUniform;
	private int colorAdjustSaturationUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadColorAdjust(GLES20 gl){
		super(gl, "Color adjust quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragColorAdjust.glsl"));
		
	}

	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		colorAdjustBrightnessUniform = GLES20.glGetUniformLocation(shaderProgramID, "colorAdjustBrightness");
		colorAdjustContrastUniform = GLES20.glGetUniformLocation(shaderProgramID, "colorAdjustContrast");
		colorAdjustSaturationUniform = GLES20.glGetUniformLocation(shaderProgramID, "colorAdjustSaturation");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setColorAdjust(GLES20 gl, float colorAdjustBrightness, float colorAdjustContrast, float colorAdjustSaturation){
		
		GLES20.glUniform1f(colorAdjustBrightnessUniform, colorAdjustBrightness);
		GLES20.glUniform1f(colorAdjustContrastUniform, colorAdjustContrast);
		GLES20.glUniform1f(colorAdjustSaturationUniform, colorAdjustSaturation);
	}
}
