package engine.renderer.android.shader.quad;

import android.opengl.GLES20;
import geometry.math.Vector2d;

public class ShaderQuadBlurRadial extends ShaderQuad{
	
	//adjustment parameters
	private int blurCenterUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadBlurRadial(GLES20 gl){
		super(gl, "Radial blur quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragBlurRadial.glsl"));
	}
	
	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		blurCenterUniform = GLES20.glGetUniformLocation(shaderProgramID, "blurCenter");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setBlurCenter(GLES20 gl, Vector2d blurCenter){
		
		GLES20.glUniform2f(blurCenterUniform, blurCenter.getX(), blurCenter.getY());
	}
}
