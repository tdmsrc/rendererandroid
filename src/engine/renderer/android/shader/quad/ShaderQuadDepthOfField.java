package engine.renderer.android.shader.quad;

import android.opengl.GLES20;

public class ShaderQuadDepthOfField extends ShaderQuad{
	
	public static final int DEPTH_TEXTURE_UNIT = 1;
	public static final int BLUR_TEXTURE_UNIT = 2;

	//depth texture 
	private int depthTextureUniform;
	private int blurTextureUniform;
	
	//adjustment parameters
	private int zNearInvUniform;
	private int zFarInvUniform;
	private int dFocusTargetUniform;
	private int dFocusRangeInvUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadDepthOfField(GLES20 gl){
		super(gl, "Depth field quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragDepthOfField.glsl"));
		
	}

	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		depthTextureUniform = GLES20.glGetUniformLocation(shaderProgramID, "texDepth");
		blurTextureUniform = GLES20.glGetUniformLocation(shaderProgramID, "texBlur");
		
		zNearInvUniform = GLES20.glGetUniformLocation(shaderProgramID, "zNearInv");
		zFarInvUniform = GLES20.glGetUniformLocation(shaderProgramID, "zFarInv");
		dFocusTargetUniform = GLES20.glGetUniformLocation(shaderProgramID, "dFocusTarget");
		dFocusRangeInvUniform = GLES20.glGetUniformLocation(shaderProgramID, "dFocusRangeInv");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GLES20 gl){
		super.useShader(gl);
		
		//set texture
		GLES20.glUniform1i(depthTextureUniform, DEPTH_TEXTURE_UNIT);
		GLES20.glUniform1i(blurTextureUniform, BLUR_TEXTURE_UNIT);
	}
	
	public void setParameters(GLES20 gl, float zNear, float zFar, float focusTargetDistance, float focusRange){
		
		GLES20.glUniform1f(zNearInvUniform, 1.0f / zNear);
		GLES20.glUniform1f(zFarInvUniform, 1.0f / zFar);
		GLES20.glUniform1f(dFocusTargetUniform, focusTargetDistance);
		GLES20.glUniform1f(dFocusRangeInvUniform, 1.0f / focusRange);
	}
}
