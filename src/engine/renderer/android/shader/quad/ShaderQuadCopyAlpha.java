package engine.renderer.android.shader.quad;

import android.opengl.GLES20;

public class ShaderQuadCopyAlpha extends ShaderQuad{
	
	//adjustment parameters
	private int alphaUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadCopyAlpha(GLES20 gl){
		super(gl, "Copy alpha quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragCopyAlpha.glsl"));
	}
	
	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		alphaUniform = GLES20.glGetUniformLocation(shaderProgramID, "alpha");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setAlpha(GLES20 gl, float alpha){
		
		GLES20.glUniform1f(alphaUniform, alpha);
	}
}
