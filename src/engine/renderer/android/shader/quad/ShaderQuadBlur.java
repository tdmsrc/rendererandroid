package engine.renderer.android.shader.quad;

import android.opengl.GLES20;

public class ShaderQuadBlur extends ShaderQuad{

	//texture data
	private int textureSizeUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public enum BlurDirection{ BLUR_DIRECTION_HORIZONTAL, BLUR_DIRECTION_VERTICAL; }
	
	public ShaderQuadBlur(GLES20 gl, BlurDirection blurDirection){
		super(gl, (blurDirection == BlurDirection.BLUR_DIRECTION_HORIZONTAL) ? "BlurH quad shader" : "BlurV quad shader", 
			ShaderQuad.class.getResourceAsStream(
				(blurDirection == BlurDirection.BLUR_DIRECTION_HORIZONTAL) ? 
				"/resource/shader/quad/ShaderQuadFragBlurH.glsl" : 
				"/resource/shader/quad/ShaderQuadFragBlurV.glsl" )
			);
	}

	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		textureSizeUniform = GLES20.glGetUniformLocation(shaderProgramID, "texSize");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setTextureSize(GLES20 gl, int textureWidth, int textureHeight){
		
		GLES20.glUniform2i(textureSizeUniform, textureWidth, textureHeight);
	}
}
