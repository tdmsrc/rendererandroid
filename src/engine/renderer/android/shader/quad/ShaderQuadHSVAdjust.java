package engine.renderer.android.shader.quad;

import android.opengl.GLES20;

public class ShaderQuadHSVAdjust extends ShaderQuad{

	//adjustment parameters
	private int hsvAdjustHueUniform;
	private int hsvAdjustSaturationUniform;
	private int hsvAdjustValueUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadHSVAdjust(GLES20 gl){
		super(gl, "HSV adjust quad shader", ShaderQuad.class.getResourceAsStream("/resource/shader/quad/ShaderQuadFragHSVAdjust.glsl"));
		
	}

	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		hsvAdjustHueUniform = GLES20.glGetUniformLocation(shaderProgramID, "hsvAdjustHue");
		hsvAdjustSaturationUniform = GLES20.glGetUniformLocation(shaderProgramID, "hsvAdjustSaturation");
		hsvAdjustValueUniform = GLES20.glGetUniformLocation(shaderProgramID, "hsvAdjustValue");
		
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setHSVAdjust(GLES20 gl, float hsvAdjustHue, float hsvAdjustSaturation, float hsvAdjustValue){
		
		GLES20.glUniform1f(hsvAdjustHueUniform, hsvAdjustHue);
		GLES20.glUniform1f(hsvAdjustSaturationUniform, hsvAdjustSaturation);
		GLES20.glUniform1f(hsvAdjustValueUniform, hsvAdjustValue);
		
	}
}
