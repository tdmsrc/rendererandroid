package engine.renderer.android.shader.quad;

import java.io.InputStream;

import android.opengl.GLES20;
import engine.renderer.android.object.GLESBuffer;
import engine.renderer.android.shader.ShaderProgram;
import engine.tools.Quad;

public class ShaderQuad extends ShaderProgram{

	public static final int COLOR_TEXTURE_UNIT = 0;
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1;

	//texture data
	private int textureColorUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuad(GLES20 gl, String name){
		super(gl, name);
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/quad/ShaderQuadVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/quad/ShaderQuadFragCopy.glsl"));
		finishAttaching(gl);
	}
	
	public ShaderQuad(GLES20 gl, String name, InputStream streamFragmentShaderSource){
		super(gl, name);
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/quad/ShaderQuadVert.glsl"));
		setPrimaryFragmentShaderSource(streamFragmentShaderSource);
		finishAttaching(gl);
	}
	
	@Override
	protected void setVertexAttributes(GLES20 gl) {

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
	}
	
	@Override
	protected void setUniforms(GLES20 gl) {
  
		//get uniform locations
	    textureColorUniform = GLES20.glGetUniformLocation(shaderProgramID, "texColor");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GLES20 gl){
		super.useShader(gl);

		//set texture
		GLES20.glUniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
	}
	
	public void setVertexAttributeBuffers(GLES20 gl, Quad<?,GLESBuffer> quad){

		//bind vertex data
		quad.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		quad.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
}
