package engine.renderer.android.shader.skybox;

import android.opengl.GLES20;
import engine.renderer.android.object.GLESBuffer;
import engine.renderer.android.shader.ShaderProgram;
import engine.scene.Skybox;
import engine.scene.Skybox.SkyboxFace;
import geometry.math.Camera;


public class ShaderSkybox extends ShaderProgram{

	public static final int SKYBOX_TEXTURE_UNIT = 0;
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1;
	
	//texture
	private int textureColorUniform;
	//transformations
	private float[] projectionMatrixArray, modelViewRotationMatrixArray;
	private int projectionMatrixUniform, modelViewRotationMatrixUniform;
	

	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderSkybox(GLES20 gl){
		super(gl, "Skybox shader");
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/skybox/ShaderSkyboxVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/skybox/ShaderSkyboxFrag.glsl"));
		finishAttaching(gl);
		
		projectionMatrixArray = new float[16];
		modelViewRotationMatrixArray = new float[9];
	}

	@Override
	protected void setVertexAttributes(GLES20 gl){

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
	}

	@Override
	protected void setUniforms(GLES20 gl){

		//get uniform locations
		textureColorUniform = GLES20.glGetUniformLocation(shaderProgramID, "texColor");
		
	    projectionMatrixUniform = GLES20.glGetUniformLocation(shaderProgramID, "projectionMatrix");
	    modelViewRotationMatrixUniform = GLES20.glGetUniformLocation(shaderProgramID, "modelViewRotation"); 
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GLES20 gl){
		super.useShader(gl);

		//set textures
		GLES20.glUniform1i(textureColorUniform, SKYBOX_TEXTURE_UNIT);
	}
	
	public void setViewMatrices(GLES20 gl, Camera camera){
		
		camera.getProjectionMatrix().serializeColumnMajor(projectionMatrixArray);
		GLES20.glUniformMatrix4fv(projectionMatrixUniform, 1, false, projectionMatrixArray, 0);
		
		camera.getRotation().getMatrix().serializeColumnMajor(modelViewRotationMatrixArray);
		GLES20.glUniformMatrix3fv(modelViewRotationMatrixUniform, 1, false, modelViewRotationMatrixArray, 0);
	}
	
	public void setVertexAttributeBuffers(GLES20 gl, Skybox<?,GLESBuffer,?> skybox, SkyboxFace face){

		//bind vertex data
		skybox.getPositionBuffer(face).bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		skybox.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
	
	public void setVertexAttributeBuffersSun(GLES20 gl, Skybox<?,GLESBuffer,?> skybox){
		
		//bind vertex data
		skybox.getSunPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		skybox.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
}
