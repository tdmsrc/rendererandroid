package engine.renderer.android.shader.lightpass;

import android.opengl.GLES20;
import engine.scene.Light;

public class ShaderLightPassDirectional extends ShaderLightPass{

	public static final int SHADOW_MAP_TEXTURE_UNIT = 0;
	
	private int textureShadowMapUniform;
	
	private float[] lightProjectionMatrixArray, lightModelViewRotationMatrixArray;
	private int lightProjectionMatrixUniform, lightModelViewRotationMatrixUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderLightPassDirectional(GLES20 gl, boolean normalMapping) {
		super(gl, normalMapping);
		
		lightProjectionMatrixArray = new float[16];
		lightModelViewRotationMatrixArray = new float[9];
	}
	
	@Override
	protected void attachShadowShaders(GLES20 gl){
		addFragmentShaderSourceIdentifier("isFragShadowed", getClass().getResourceAsStream("/resource/shader/lightpass/IsShadowedDirectional.glsl"));
	}

	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
		textureShadowMapUniform = GLES20.glGetUniformLocation(shaderProgramID, "texShadowMap");
		
		lightProjectionMatrixUniform = GLES20.glGetUniformLocation(shaderProgramID, "lightProjectionMatrix");
		lightModelViewRotationMatrixUniform = GLES20.glGetUniformLocation(shaderProgramID, "lightModelViewRotation");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GLES20 gl){
		super.useShader(gl);
		
		GLES20.glUniform1i(textureShadowMapUniform, SHADOW_MAP_TEXTURE_UNIT);
	}
	
	public void setAmbientToShadowMapTransformation(GLES20 gl, Light light){
		
		light.getCamera().getRotation().getMatrix().serializeColumnMajor(lightModelViewRotationMatrixArray);
		GLES20.glUniformMatrix3fv(lightModelViewRotationMatrixUniform, 1, false, lightModelViewRotationMatrixArray, 0);
		
		light.getCamera().getProjectionMatrix().serializeColumnMajor(lightProjectionMatrixArray);
		GLES20.glUniformMatrix4fv(lightProjectionMatrixUniform, 1, false, lightProjectionMatrixArray, 0);
	}
}
