package engine.renderer.android.shader.lightpass;

import android.opengl.GLES20;
import engine.renderer.Drawable;
import engine.renderer.android.object.GLESBuffer;
import engine.scene.DrawOptions;
import geometry.math.Vector3d;

//similar to ShaderLightPass,
//but with ambient light data instead of light, and additional emissive light data

public class ShaderAmbientAndEmissive extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1;
	public static final int BUMP_TEXTURE_UNIT = 2; //[TODO] only used if normalMapping == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1,
		VERTEX_NORMAL_ATTRIBUTE = 2,
		VERTEX_TANGENT_S_ATTRIBUTE = 3, //[TODO] only used if normalMapping == true
		VERTEX_TANGENT_T_ATTRIBUTE = 4; //[TODO] only used if normalMapping == true
	
	//options for attaching shaders
	private static final OptionParallax optionParallax = OptionParallax.NONE; //PARALLAX_OCCLUSION_MAPPING;
	
	//texture/material data
	private int textureColorUniform;
	private int textureBumpUniform; //[TODO] only used if normalMapping == true
	private int reliefMappingHeightUniform; //[TODO] only used if normalMapping == true
	//light options
	private int ambientCoefficientUniform, ambientColorUniform;
	private int emissiveCoefficientUniform, emissiveColorUniform;
	private int fogDensityUniform;
	
	//shader options
	private boolean normalMapping;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderAmbientAndEmissive(GLES20 gl, boolean normalMapping){
		super(gl, "Ambient and emissive shader [normalMapping: " + normalMapping + "]");
		
		this.normalMapping = normalMapping;
		
		addVertexShaderSourceIdentifier("getFogIntensity", getClass().getResourceAsStream("/resource/shader/lightpass/FogIntensity.glsl"));
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderAmbientAndEmissiveVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderAmbientAndEmissiveFrag.glsl"));
		
		if(normalMapping){
			
			addVertexShaderSourceIdentifier("setTangentVectors", getClass().getResourceAsStream("/resource/shader/lightpass/VaryingTangentsTextureVert.glsl"));
			switch(optionParallax){
			case NONE:
				addFragmentShaderSourceIdentifier("getParallax", getClass().getResourceAsStream("/resource/shader/lightpass/ParallaxNone.glsl")); break;
			case PARALLAX_MAPPING:
				addFragmentShaderSourceIdentifier("getParallax", getClass().getResourceAsStream("/resource/shader/lightpass/ParallaxMapping.glsl")); break;
			case PARALLAX_OCCLUSION_MAPPING:
				addFragmentShaderSourceIdentifier("getParallax", getClass().getResourceAsStream("/resource/shader/lightpass/ParallaxOcclusionMapping.glsl")); break;
			}
		}else{
			addVertexShaderSourceIdentifier("setTangentVectors", getClass().getResourceAsStream("/resource/shader/lightpass/VaryingTangentsNoneVert.glsl"));
			addFragmentShaderSourceIdentifier("getParallax", getClass().getResourceAsStream("/resource/shader/lightpass/ParallaxNone.glsl"));
		}
		
		finishAttaching(gl);
	}

	@Override
	protected void setVertexAttributes(GLES20 gl) {
		
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		addVertexAttribute(gl, "vertexNormal", VERTEX_NORMAL_ATTRIBUTE);
		if(normalMapping){
			addVertexAttribute(gl, "vertexTangentS", VERTEX_TANGENT_S_ATTRIBUTE);
			addVertexAttribute(gl, "vertexTangentT", VERTEX_TANGENT_T_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(GLES20 gl) {
	    super.setUniforms(gl);
		
	    textureColorUniform = GLES20.glGetUniformLocation(shaderProgramID, "texColor");
	    if(normalMapping){
	    	textureBumpUniform = GLES20.glGetUniformLocation(shaderProgramID, "texBump"); 
	    	reliefMappingHeightUniform = GLES20.glGetUniformLocation(shaderProgramID, "reliefMappingHeight");
	    }

	    //light uniforms
	    ambientCoefficientUniform = GLES20.glGetUniformLocation(shaderProgramID, "ambientCoefficient");
	    ambientColorUniform = GLES20.glGetUniformLocation(shaderProgramID, "ambientColor");
	    emissiveCoefficientUniform = GLES20.glGetUniformLocation(shaderProgramID, "emissiveCoefficient");
	    emissiveColorUniform = GLES20.glGetUniformLocation(shaderProgramID, "emissiveColor");
	    
	    fogDensityUniform = GLES20.glGetUniformLocation(shaderProgramID, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GLES20 gl){
		super.useShader(gl);

		//set texture
		GLES20.glUniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		if(normalMapping){
			GLES20.glUniform1i(textureBumpUniform, BUMP_TEXTURE_UNIT);
		}
	}
	
	public void setFog(GLES20 gl, float fogDensity){
		
		//set fog density
		GLES20.glUniform1f(fogDensityUniform, fogDensity);
	}
	
	public void setAmbient(GLES20 gl, Vector3d ambientColor, float ambientCoefficient){
		
		//set material lighting coefficient uniforms
		GLES20.glUniform3f(ambientColorUniform, ambientColor.getX(), ambientColor.getY(), ambientColor.getZ());
		GLES20.glUniform1f(ambientCoefficientUniform, ambientCoefficient);
	}
	
	public void setEmissive(GLES20 gl, DrawOptions drawOptions){
		
		if(drawOptions.checkFlags(DrawOptions.MASK_EMISSIVE, DrawOptions.MASK_EMISSIVE)){
			Vector3d emissiveColor = drawOptions.getEmissiveColor();
			GLES20.glUniform3f(emissiveColorUniform, emissiveColor.getX(), emissiveColor.getY(), emissiveColor.getZ());
			GLES20.glUniform1f(emissiveCoefficientUniform, drawOptions.getEmissiveCoefficient());
		}else{
			GLES20.glUniform3f(emissiveColorUniform, ZERO_VECTOR.getX(), ZERO_VECTOR.getY(), ZERO_VECTOR.getZ());
			GLES20.glUniform1f(emissiveCoefficientUniform, 0.0f);
		}
	}
	
	public void setReliefMappingHeight(GLES20 gl, float reliefMappingHeight){
		
		//set relief mapping height
		GLES20.glUniform1f(reliefMappingHeightUniform, reliefMappingHeight);
	}
	
	public void setVertexAttributeBuffers(GLES20 gl, Drawable<?,GLESBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		drawable.getNormalBuffer().bindAsVertexAttrib(gl, VERTEX_NORMAL_ATTRIBUTE);
		if(normalMapping){
			drawable.getTangentSBuffer().bindAsVertexAttrib(gl, VERTEX_TANGENT_S_ATTRIBUTE);
			drawable.getTangentTBuffer().bindAsVertexAttrib(gl, VERTEX_TANGENT_T_ATTRIBUTE);
		}
	}
}
