package engine.renderer.android.shader.lightpass;

import android.opengl.GLES20;
import engine.renderer.Drawable;
import engine.renderer.android.object.GLESBuffer;
import geometry.math.Vector3d;


public class ShaderZPrepass extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1; //[TODO] only used if useAlpha == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1; //[TODO] only used if useAlpha == true
	
	//texture/material data
	private int textureColorUniform; //[TODO] only used if useAlpha == true
	private int opacityUniform; //[TODO] only used if useAlpha == true
	//fog color
	private int fogColorUniform, fogDensityUniform;
	
	//shader options
	private boolean useAlpha; 
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderZPrepass(GLES20 gl, boolean useAlpha){
		super(gl, "Z-Prepass shader [useAlpha: " + useAlpha + "]");
		
		this.useAlpha = useAlpha;
		
		addVertexShaderSourceIdentifier("getFogIntensity", getClass().getResourceAsStream("/resource/shader/lightpass/FogIntensity.glsl"));
		
		if(useAlpha){
			setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderZPrepassAlphaVert.glsl"));
			setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderZPrepassAlphaFrag.glsl"));
		}else{
			setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderZPrepassNoAlphaVert.glsl"));
			setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderZPrepassNoAlphaFrag.glsl"));
		}
		
		finishAttaching(gl);
	}
	
	@Override
	protected void setVertexAttributes(GLES20 gl) {

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		if(useAlpha){
			addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);

		if(useAlpha){
			textureColorUniform = GLES20.glGetUniformLocation(shaderProgramID, "texColor");
			opacityUniform = GLES20.glGetUniformLocation(shaderProgramID, "opacity");
		}
		
	    fogColorUniform = GLES20.glGetUniformLocation(shaderProgramID, "fogColor");
	    fogDensityUniform = GLES20.glGetUniformLocation(shaderProgramID, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GLES20 gl){
		super.useShader(gl);

		//set texture
		if(useAlpha){
			GLES20.glUniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		}
	}
	
	public void setOpacity(GLES20 gl, float opacity){
		
		//set opacity
		GLES20.glUniform1f(opacityUniform, opacity);
	}

	public void setFog(GLES20 gl, Vector3d fogColor, float fogDensity){
		
		//set fog color uniform
		GLES20.glUniform3f(fogColorUniform, fogColor.getX(), fogColor.getY(), fogColor.getZ());
		GLES20.glUniform1f(fogDensityUniform, fogDensity);
	}
	
	public void setVertexAttributeBuffers(GLES20 gl, Drawable<?,GLESBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		if(useAlpha){
			drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		}
	}
}
