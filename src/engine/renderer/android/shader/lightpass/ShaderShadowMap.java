package engine.renderer.android.shader.lightpass;

import android.opengl.GLES20;
import engine.renderer.Drawable;
import engine.renderer.android.object.GLESBuffer;


public class ShaderShadowMap extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1; //[TODO] only used if alphaMasking == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1; //[TODO] only used if alphaMasking == true
	
	//texture/material data
	private int textureColorUniform; //[TODO] only used if alphaMasking == true
	
	//shader options
	private boolean alphaMasking;

	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderShadowMap(GLES20 gl, boolean alphaMasking){
		super(gl, "Shadow map shader [alphaMasking: " + alphaMasking + "]");
		
		this.alphaMasking = alphaMasking;
		
		if(alphaMasking){
			setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderShadowMapMaskVert.glsl"));
			setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderShadowMapMaskFrag.glsl"));
		}else{
			setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderShadowMapNoMaskVert.glsl"));
			setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderShadowMapNoMaskFrag.glsl"));
		}
		
		finishAttaching(gl);
	}

	@Override
	protected void setVertexAttributes(GLES20 gl){
	
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		if(alphaMasking){
			addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
		if(alphaMasking){
			textureColorUniform = GLES20.glGetUniformLocation(shaderProgramID, "texColor");
		}
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(GLES20 gl){
		super.useShader(gl);
		
		//set texture
		if(alphaMasking){
			GLES20.glUniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		}
	}
	
	public void setVertexAttributeBuffers(GLES20 gl, Drawable<?,GLESBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		if(alphaMasking){
			drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		}
	}
}
