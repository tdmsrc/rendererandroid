package engine.renderer.android.shader.lightpass;

import android.opengl.GLES20;
import engine.renderer.Drawable;
import engine.renderer.android.object.GLESBuffer;
import geometry.math.Vector3d;


public class ShaderWireframe extends ShaderGeneric{

	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0;
	
	//wireframe color
	private int colorUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderWireframe(GLES20 gl){
		super(gl, "Wireframe shader");
		
		setPrimaryVertexShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderWireframeVert.glsl"));
		setPrimaryFragmentShaderSource(getClass().getResourceAsStream("/resource/shader/lightpass/ShaderWireframeFrag.glsl"));
		
		finishAttaching(gl);
	}

	@Override
	protected void setVertexAttributes(GLES20 gl) {
		
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
	}

	@Override
	protected void setUniforms(GLES20 gl) {
		super.setUniforms(gl);
		
	    colorUniform = GLES20.glGetUniformLocation(shaderProgramID, "wireframeColor");
	}

	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================

	public void setColor(GLES20 gl, Vector3d color){
		
		//set color uniform
		GLES20.glUniform3f(colorUniform, color.getX(), color.getY(), color.getZ());
	}
	
	public void setVertexAttributeBuffers(GLES20 gl, Drawable<?,GLESBuffer> drawable){

		drawable.getWireframeBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
	}
}
