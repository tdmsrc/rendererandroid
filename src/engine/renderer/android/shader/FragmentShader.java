package engine.renderer.android.shader;

import android.opengl.GLES20;
import geometry.common.MessageOutput;


public class FragmentShader extends Shader{

	public FragmentShader(GLES20 gl, String fragmentShaderSource){
		super(GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER));
		
		MessageOutput.printDebug("Compiling fragment shader");
		GLES20.glShaderSource(shaderID, fragmentShaderSource);
		GLES20.glCompileShader(shaderID);
		if(CHECK_ERRORS){ checkCompileStatus(gl); }
	}
}