package engine.renderer.android.shader;

import android.opengl.GLES20;
import geometry.common.MessageOutput;


public class VertexShader extends Shader{

	public VertexShader(GLES20 gl, String vertexShaderSource){
		super(GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER));
		
		MessageOutput.printDebug("Compiling vertex shader");
		GLES20.glShaderSource(shaderID, vertexShaderSource);
		GLES20.glCompileShader(shaderID);
		if(CHECK_ERRORS){ checkCompileStatus(gl); }
	}
}