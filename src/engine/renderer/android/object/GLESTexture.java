package engine.renderer.android.object;

import java.io.InputStream;
import java.nio.ByteBuffer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import engine.renderer.GenericTexture;
import engine.renderer.android.buffer.BufferColor;
import engine.renderer.android.shader.quad.ShaderQuad;
import engine.tools.Quad;
import engine.toolsAWT.BufferTexture;
import geometry.common.MessageOutput;
import geometry.math.Vector4d;


public class GLESTexture extends GenericTexture<GLES20>{

	private int width, height;
	private int texID;
	
	
	public GLESTexture(int texID, int width, int height){
		
		this.texID = texID;
		this.width = width;
		this.height = height;
	}

	
	public GLESTexture(GLES20 gl, InputStream inputStream, String format, 
		boolean generateMipmaps, boolean wrap){

		//generate textures
		int[] createTexIDs = new int[1];
		GLES20.glGenTextures(createTexIDs.length, createTexIDs, 0);
		texID = createTexIDs[0];    
		
		//populate the texture data
		initializeTexture(gl, inputStream, format, texID, generateMipmaps, wrap);
	}
	
	@Override
	public void delete(GLES20 gl){
		
		int[] hArray = new int[]{ texID };
		GLES20.glDeleteTextures(hArray.length, hArray, 0);
		
		MessageOutput.printDebug("Deleted texture");
	}
	
	private void initializeTexture(GLES20 gl, InputStream inputStream, String format, int texID, 
		boolean generateMipmaps, boolean wrap){
		
		MessageOutput.printDebug("Initializing texture ...");
		
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inScaled = false;
		
		//TODO this seems rather inefficient
		Bitmap bitmapUL = BitmapFactory.decodeStream(inputStream, null, opts); //has origin in upper left
		Matrix flipMatrix = new Matrix();
		flipMatrix.postScale(1.0f,  -1.0f);
		Bitmap bitmap = Bitmap.createBitmap(bitmapUL, 0, 0, bitmapUL.getWidth(), bitmapUL.getHeight(), flipMatrix, true);
		bitmapUL.recycle();
		
		//store width and height
		this.width = bitmap.getWidth();
		this.height = bitmap.getHeight();
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texID);
		
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, wrap ? GLES20.GL_REPEAT : GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, wrap ? GLES20.GL_REPEAT : GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, 
			generateMipmaps ? GLES20.GL_LINEAR_MIPMAP_LINEAR : GLES20.GL_LINEAR);
		
		GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
		if(generateMipmaps){ GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D); }
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
		
		bitmap.recycle();
	}
	
	@Override
	public int getWidth(){ return width; }
	
	@Override
	public int getHeight(){ return height; }
	
	
	public void bind(GLES20 gl, int texUnitColor){

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0+texUnitColor);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texID);
	}
	
	public void unbind(GLES20 gl, int texUnitColor){
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0+texUnitColor);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
	}

	public void bindReadFrom(GLES20 gl){
		
		//attach texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, texID, 0);
		
		//specify drawbuffers and readbuffers
		//TODO: not necessary on GLES2?
		//GLES20.glDrawBuffer(GLES20.GL_NONE);
		//GLES20.glReadBuffer(GLES20.GL_COLOR_ATTACHMENT0); 
		
		//check if it worked
		int status = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
		if(status != GLES20.GL_FRAMEBUFFER_COMPLETE){ 
			throw new Error("FBO status is not GL_FRAMEBUFFER_COMPLETE."); }
		
		//set appropriate viewport
		GLES20.glViewport(0, 0, width, height);
	}
	
	public void unbindReadFrom(GLES20 gl){
		
		//detach texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, 0, 0);
	}
	
	/**
	 * Uses glReadPixels, which is slow.  Only use this if it is really necessary.
	 */
	@Override
	public void getAsBufferTexture(GLES20 gl, BufferTexture target, 
		int srcMinX, int srcMinY, int srcWidth, int srcHeight){
		
		//[TODO] can do e.g. gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, 1) instead, 
		//but maybe there is a reason not to; is it slower?
		srcWidth += (4 - (srcWidth % 4)) % 4;
		srcHeight += (4 - (srcHeight % 4)) % 4;
		
		//get buffer
		ByteBuffer data = target.getBufferForWriting(srcWidth, srcHeight);
		
		//read in the rect of pixel data
		bindReadFrom(gl);
		GLES20.glReadPixels(srcMinX, srcMinY, srcWidth, srcHeight, 
			GLES11Ext.GL_BGRA, GLES20.GL_UNSIGNED_BYTE, data);
		unbindReadFrom(gl);
	}
	
	/**
	 * Uses glReadPixels, which is slow.  Only use this if it is really necessary.
	 */
	@Override
	public void getAsBufferTexture(GLES20 gl, BufferTexture target, 
		int srcMinX, int srcMinY, int srcWidth, int srcHeight,
		int targetScaledWidth, int targetScaledHeight){
		
		//find appropriate scaling
		float sx = (srcWidth <= targetScaledWidth) ? 1.0f : (float)targetScaledWidth/(float)srcWidth;
		float sy = (srcHeight <= targetScaledHeight) ? 1.0f : (float)targetScaledHeight/(float)srcHeight;
		float s = (sx < sy) ? sx : sy;
		
		int scaledWidth = (int)(srcWidth * s);
		int scaledHeight = (int)(srcHeight * s);
		
		//ensure target size has dims multiple of 4
		scaledWidth += (4 - (scaledWidth % 4)) % 4;
		scaledHeight += (4 - (scaledHeight % 4)) % 4;
		
		//create stuff necessary to draw texture onto a BufferColor at requested size
		BufferColor bufThumb = new BufferColor(gl, scaledWidth, scaledHeight);
		Quad<GLES20,GLESBuffer> quad = new Quad<GLES20,GLESBuffer>(gl, GLESBuffer.getConstructor());
		ShaderQuad shader = new ShaderQuad(gl, "Copy quad shader");
		
		//prepare bufThumb for drawing to
		bufThumb.clearColor(gl, new Vector4d(1,0,1,1));
		bufThumb.bindDrawTo(gl);
		//draw this texture onto bufThumb (this is exactly drawBuffer in main Renderer routine)
		shader.useShader(gl);
		shader.setVertexAttributeBuffers(gl, quad);
		bind(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3*quad.getTriCount());
		unbind(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		shader.unuseShader(gl);
		//unbind bufThumb
		bufThumb.unbindDrawTo(gl);
		
		//read bufThumb into target BufferTexture
		ByteBuffer data = target.getBufferForWriting(scaledWidth, scaledHeight);
		
		bufThumb.bindReadFrom(gl);
		GLES20.glReadPixels(0, 0, scaledWidth, scaledHeight, 
			GLES11Ext.GL_BGRA, GLES20.GL_UNSIGNED_BYTE, data);
		bufThumb.unbindReadFrom(gl);
		
		//delete created stuff
		bufThumb.delete(gl);
		//[TODO] delete quad
		//[TODO] delete shader
	}
}