package engine.renderer.android.object;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.GLES20;
import engine.renderer.GenericBuffer;
import geometry.common.MessageOutput;


public class GLESBuffer extends GenericBuffer<GLES20>{

	private static final int BYTES_PER_SHORT = 2;
	private static final int BYTES_PER_FLOAT = 4;
	
	private int bufferID;
	private int floatsPerAttribute; //only used if created as vertex attribute buffer
	
	//constructor
	public static class Constructor implements BufferConstructor<GLES20, GLESBuffer>{
		
		@Override
		public GLESBuffer construct(GLES20 context, float[] fArray, int floatsPerAttribute){
			return new GLESBuffer(context, fArray, floatsPerAttribute);
		}
		
		@Override
		public GLESBuffer construct(GLES20 context, int[] iArray){
			return new GLESBuffer(context, iArray);
		}
	}
	protected static Constructor constructor = new Constructor();
	public static Constructor getConstructor(){ return constructor; }
	
	
	public GLESBuffer(GLES20 gl, float[] fArray, int floatsPerAttribute){
	
		this.floatsPerAttribute = floatsPerAttribute;
		bufferID = createAndFillVertexAttribBuffer(gl, fArray);
	}
	
	public GLESBuffer(GLES20 gl, int[] iArray){
		
		bufferID = createAndFillElementArrayBuffer(gl, iArray);
	}
	
	private static int createAndFillVertexAttribBuffer(GLES20 gl, float[] fArray){
		
		int[] hArray = new int[1];
		GLES20.glGenBuffers(1, hArray, 0);
		int h = hArray[0];
		
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, h);
		
		FloatBuffer data = FloatBuffer.allocate(fArray.length); //BufferUtil.newFloatBuffer(fArray.length);
		for(int i=0; i<fArray.length; i++){ data.put(i,fArray[i]); }
		//TODO: why doesnt data.put(fArray) work?
		GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, fArray.length*BYTES_PER_FLOAT, data, GLES20.GL_STATIC_DRAW);
		
		return h;
	}
	
	private static int createAndFillElementArrayBuffer(GLES20 gl, int[] iArray){
		
		int[] hArray = new int[1];
		GLES20.glGenBuffers(1, hArray, 0);
		int h = hArray[0];
		
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, h);
		
		ShortBuffer data = ShortBuffer.allocate(iArray.length);
		for(int i=0; i<iArray.length; i++){ data.put(i,(short)iArray[i]); }
		//TODO: why doesnt data.put(fArray) work?
		GLES20.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER, iArray.length*BYTES_PER_SHORT, data, GLES20.GL_STATIC_DRAW);
		
		return h;
	}
	
	//[TODO] buffer modification
	//public void modify(GL2 gl, long offset, long size, Buffer data){
	//	
	//	gl.glBufferSubData(bufferID, offset, size, data);
	//}
	
	@Override
	public void delete(GLES20 gl){
		
		int[] hArray = new int[]{ bufferID };
		GLES20.glDeleteBuffers(hArray.length, hArray, 0);
		
		MessageOutput.printDebug("Deleted array buffer");
	}
	
	//bind
	public void bindAsElementArray(GLES20 gl){
		
		GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, bufferID);
	}
	
	public void bindAsVertexAttrib(GLES20 gl, int vertexAttributeIndex){ 
		
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, bufferID);
		GLES20.glVertexAttribPointer(vertexAttributeIndex, floatsPerAttribute, GLES20.GL_FLOAT, false, 0, 0);
	}
}