package engine.renderer.android.buffer;

import android.opengl.GLES20;
import geometry.common.MessageOutput;
import geometry.math.Vector4d;


public class BufferColor implements DrawBuffer{
	 
	private int width, height;
	private int texidColor;
	
	
	public BufferColor(GLES20 gl, int width, int height){
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	@Override
	public int getHeight(){ return height; }

	@Override
	public int getWidth(){ return width; }
	
	@Override
	public void resize(GLES20 gl, int width, int height){

		delete(gl);
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	private void create(GLES20 gl){

		//generate textures
		int[] createTexIDs = new int[1];
		GLES20.glGenTextures(createTexIDs.length, createTexIDs, 0);
		texidColor = createTexIDs[0];
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texidColor);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, width, height, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
		
		//check for errors
		switch(GLES20.glGetError()){
		case GLES20.GL_INVALID_ENUM: throw new Error("(BufferColor) GL error GL_INVALID_ENUM");
		case GLES20.GL_INVALID_VALUE: throw new Error("(BufferColor) GL error GL_INVALID_VALUE");
		case GLES20.GL_INVALID_OPERATION: throw new Error("(BufferColor) GL error GL_INVALID_OPERATION");
		case GLES20.GL_INVALID_FRAMEBUFFER_OPERATION: throw new Error("(BufferColor) GL error GL_INVALID_FRAMEBUFFER_OPERATION");
		case GLES20.GL_OUT_OF_MEMORY: throw new Error("(BufferColor) GL error GL_OUT_OF_MEMORY");
		}
		
		MessageOutput.printDebug("Created draw buffer at " + width + "x" + height);
	}
	
	@Override
	public void delete(GLES20 gl){

		int[] delTexIDs = new int[]{ texidColor };
		GLES20.glDeleteTextures(delTexIDs.length, delTexIDs, 0);
		
		MessageOutput.printDebug("Deleted draw buffer");
	}

	@Override
	public void clearDepth(GLES20 gl){ }
	
	@Override
	public void clearColor(GLES20 gl, Vector4d color){
		
		GLES20.glColorMask(true, true, true, true);
		
		GLES20.glClearColor(color.getX(), color.getY(), color.getZ(), color.getW());
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void clearDepthAndColor(GLES20 gl, Vector4d color){
		clearColor(gl, color);
	}
	
	@Override
	public void bindDrawTo(GLES20 gl){
		
		//ensure render buffer is not attached
		GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_RENDERBUFFER, 0);
		
		//attach texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, texidColor, 0);
		
		//specify drawbuffers and readbuffers (unnecessary on GLES2)
		//GLES20.glDrawBuffer(GLES20.GL_COLOR_ATTACHMENT0); 
		//GLES20.glReadBuffer(GLES20.GL_NONE);
		
		//check if it worked
		int status = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
		if(status != GLES20.GL_FRAMEBUFFER_COMPLETE){
			switch(status){
			case GLES20.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: 
				throw new Error("(BufferColor) FBO status is GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT."); 
			case GLES20.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS: 
				throw new Error("(BufferColor) FBO status is GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS.");
			case GLES20.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: 
				throw new Error("(BufferColor) FBO status is GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT.");
			case GLES20.GL_FRAMEBUFFER_UNSUPPORTED: 
				throw new Error("(BufferColor) FBO status is GL_FRAMEBUFFER_UNSUPPORTED.");
			default: 
				throw new Error("(BufferColor) FBO status is not GL_FRAMEBUFFER_COMPLETE.");
			} 
		}
			
		//set appropriate viewport
		GLES20.glViewport(0, 0, width, height);
	}
	
	@Override
	public void unbindDrawTo(GLES20 gl){

		//detach texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, 0, 0);
	}
	
	public void bindReadFrom(GLES20 gl){
		
		//attach texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, texidColor, 0);
		
		//specify drawbuffers and readbuffers 
		//TODO: not necessary on GLES2?
		//GLES20.glDrawBuffer(GLES20.GL_NONE);
		//GLES20.glReadBuffer(GLES20.GL_COLOR_ATTACHMENT0); 
		
		//check if it worked
		int status = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
		if(status != GLES20.GL_FRAMEBUFFER_COMPLETE){ 
			throw new Error("FBO status is not GL_FRAMEBUFFER_COMPLETE."); }
		
		//set appropriate viewport
		GLES20.glViewport(0, 0, width, height);
	}
	
	public void unbindReadFrom(GLES20 gl){
		
		//detach texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, 0, 0);
	}
	
	@Override
	public void bindAsTexture(GLES20 gl, int texUnit){
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0+texUnit);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texidColor);
	}
	
	@Override
	public void unbindAsTexture(GLES20 gl, int texUnit){

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0+texUnit);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
	}
}