package engine.renderer.android.buffer;

import android.opengl.GLES20;
import geometry.common.MessageOutput;


public class BufferShadowMap{

	private int width, height;
	
	private int texidDepth;

	
	public BufferShadowMap(GLES20 gl, int width, int height){

		this.width = width;
		this.height = height;

		create(gl);
	}

	public int getHeight(){ return height; }
	public int getWidth(){ return width; }
	
	public void resize(GLES20 gl, int width, int height){

		delete(gl);
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	private void create(GLES20 gl){
		
		//create texture
		int[] createTexIDs = new int[1];
		GLES20.glGenTextures(createTexIDs.length, createTexIDs, 0);
		texidDepth = createTexIDs[0];
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texidDepth);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_DEPTH_COMPONENT, width, height, 0, GLES20.GL_DEPTH_COMPONENT, GLES20.GL_UNSIGNED_INT, null);
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
		
		//check for errors
		switch(GLES20.glGetError()){
		case GLES20.GL_INVALID_ENUM: throw new Error("(BufferShadowMap) GL error GL_INVALID_ENUM");
		case GLES20.GL_INVALID_VALUE: throw new Error("(BufferShadowMap) GL error GL_INVALID_VALUE");
		case GLES20.GL_INVALID_OPERATION: throw new Error("(BufferShadowMap) GL error GL_INVALID_OPERATION");
		case GLES20.GL_INVALID_FRAMEBUFFER_OPERATION: throw new Error("(BufferShadowMap) GL error GL_INVALID_FRAMEBUFFER_OPERATION");
		case GLES20.GL_OUT_OF_MEMORY: throw new Error("(BufferShadowMap) GL error GL_OUT_OF_MEMORY");
		}
		
		MessageOutput.printDebug("Created shadow map buffer at " + width + "x" + height);
	}
	
	public void delete(GLES20 gl){
		
		int[] delTexIDs = new int[]{ texidDepth };
		GLES20.glDeleteTextures(delTexIDs.length, delTexIDs, 0);
		
		MessageOutput.printDebug("Deleted directional shadow map");
	}

	public void clearDepth(GLES20 gl){

		GLES20.glDepthMask(true); 
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT);
	}
	
	public void bindDrawTo(GLES20 gl){

		//attach texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_TEXTURE_2D, texidDepth, 0);
		
		//specify drawbuffers and readbuffers
		//[TODO] for GLES2, use GL_OES_depth_texture?
		//GLES20.glDrawBuffer(GL2.GL_NONE);
		//GLES20.glReadBuffer(GL2.GL_NONE);
		
		//check if it worked
		int status = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
		if(status != GLES20.GL_FRAMEBUFFER_COMPLETE){
			switch(status){
			case GLES20.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: 
				throw new Error("(BufferShadowMap) FBO status is GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT."); 
			case GLES20.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS: 
				throw new Error("(BufferShadowMap) FBO status is GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS.");
			case GLES20.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: 
				throw new Error("(BufferShadowMap) FBO status is GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT.");
			case GLES20.GL_FRAMEBUFFER_UNSUPPORTED: 
				throw new Error("(BufferShadowMap) FBO status is GL_FRAMEBUFFER_UNSUPPORTED.");
			default: 
				throw new Error("(BufferShadowMap) FBO status is not GL_FRAMEBUFFER_COMPLETE.");
			} 
		}
		
		//set appropriate viewport
		GLES20.glViewport(0, 0, width, height);
	}
	
	public void unbindDrawTo(GLES20 gl){
		
		//detach texture
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, 
			GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_TEXTURE_2D, 0, 0);
	}
	
	public void bindAsTexture(GLES20 gl, int texUnit){
		
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0+texUnit);
	    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texidDepth);
	}
	
	public void unbindAsTexture(GLES20 gl, int texUnit){

		GLES20.glActiveTexture(GLES20.GL_TEXTURE0+texUnit);
	    GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
	}
}
