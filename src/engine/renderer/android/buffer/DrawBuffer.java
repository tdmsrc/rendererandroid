package engine.renderer.android.buffer;

import android.opengl.GLES20;
import geometry.math.Vector4d;


public interface DrawBuffer{
	
	public int getWidth();
	public int getHeight();

	public void resize(GLES20 gl, int width, int height);
	public void delete(GLES20 gl);
	
	public void clearDepth(GLES20 gl);
	public void clearColor(GLES20 gl, Vector4d color);
	public void clearDepthAndColor(GLES20 gl, Vector4d color);
	
	public void bindDrawTo(GLES20 gl);
	public void unbindDrawTo(GLES20 gl);
	
	public void bindAsTexture(GLES20 gl, int texUnit);
	public void unbindAsTexture(GLES20 gl, int texUnit);
}
