package engine.renderer.android.buffer;

import android.opengl.GLES20;
import geometry.common.MessageOutput;


public class FramebufferObject{

	private int FBOid;
	
	public FramebufferObject(GLES20 gl){
		
		//create FBO
		int[] createFBOIDs = new int[1];
		GLES20.glGenFramebuffers(createFBOIDs.length, createFBOIDs, 0);
		
		FBOid = createFBOIDs[0];
	}
	
	public void delete(GLES20 gl){
		
		int[] delFBOIDs = new int[]{ FBOid };
		GLES20.glDeleteFramebuffers(delFBOIDs.length, delFBOIDs, 0);
		
		MessageOutput.printDebug("Deleted FBO");
	}

	public void bind(GLES20 gl){
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, FBOid);
	}
	
	public void unbind(GLES20 gl){
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
	}

}
